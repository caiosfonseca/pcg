// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MapColumn.generated.h"

/**
 * 
 */
USTRUCT()
struct TG_API UMapColumn
{
	GENERATED_BODY()

	UPROPERTY()
	int Column[];
	
	UMapColumn()
	{
		Column = Null;
	}
};
